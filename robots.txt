User-Agent: *
Disallow: /

User-Agent: Googlebot
Allow: /*

User-Agent: Googlebot-Mobile
Allow: /*

Sitemap: https://vebo.ga/sitemap.xml